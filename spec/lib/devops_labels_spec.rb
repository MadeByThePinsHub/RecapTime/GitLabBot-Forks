require 'spec_helper'
require 'devops_labels'

RSpec.describe DevopsLabels do
  describe '.stages' do
    it 'returns the stages' do
      stages = described_class.stages

      expect(stages).to include('manage', 'enablement')
    end
  end

  describe '.groups_per_stage' do
    it 'returns a { stage => groups } hash' do
      groups_per_stage = described_class.groups_per_stage

      expect(groups_per_stage.keys).to match_array(described_class.stages)
    end
  end

  describe '.categories_per_group' do
    it 'returns a { group => categories } hash' do
      categories_per_group = described_class.categories_per_group

      expect(categories_per_group.keys).to match_array(described_class.groups)
    end
  end

  describe '.groups' do
    it 'returns the groups' do
      groups = described_class.groups

      expect(groups).to include('access', 'ecosystem')
    end
  end
  describe '.stage_and_departments_labels' do
    it 'returns the stage labels' do
      labels = described_class.stage_and_departments_labels

      expect(labels).to include('devops::manage', 'Quality')
    end
  end

  describe '.group_labels' do
    it 'returns the group labels' do
      labels = described_class.group_labels

      expect(labels).to include('group::access', 'group::ecosystem')
    end
  end

  describe '.category_labels' do
    it 'returns the category labels' do
      labels = described_class.category_labels

      expect(labels).to include('Category:Audit Management', 'static analysis')
    end
  end

  describe '.team_labels' do
    it 'returns the team labels' do
      labels = described_class.team_labels

      expect(labels.first).to eq('Manage [DEPRECATED]')
      expect(labels.last).to eq('Ecosystem [DEPRECATED]')
    end
  end

  describe '.departments' do
    it 'returns the departments' do
      departments = described_class.departments

      expect(departments.first).to eq('Quality')
    end
  end

  describe '.department?' do
    it 'returns true when given a department' do
      expect(described_class.department?('Quality')).to eq(true)
    end

    it 'returns true when given a department' do
      expect(described_class.department?('Foo')).to eq(false)
    end
  end

  describe described_class::Context do
    let(:resource_klass) do
      Struct.new(:labels) do
        include DevopsLabels::Context
      end
    end
    let(:label_klass) do
      Struct.new(:name)
    end
    let(:resource) { resource_klass.new([]) }

    describe '#label_names' do
      it 'returns [] if the resource has no label' do
        resource = resource_klass.new([])

        expect(resource.label_names).to eq([])
      end

      it 'returns the label names if the resource has labels' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.label_names).to eq([DevopsLabels.stage_and_departments_labels.first])
      end
    end

    describe '#current_stage_label' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_label).to be_nil
      end

      it 'returns the stage label if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.current_stage_label).to eq(DevopsLabels.stage_and_departments_labels.first)
      end
    end

    describe '#current_group_label' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_label).to be_nil
      end

      it 'returns the group label if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.current_group_label).to eq(DevopsLabels.group_labels.first)
      end
    end

    describe '#current_category_labels' do
      it 'returns [] if the resource has no category label' do
        resource = resource_klass.new([])

        expect(resource.current_category_labels).to eq([])
      end

      it 'returns the category labels if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.current_category_labels).to eq([DevopsLabels.category_labels.first])
      end
    end

    describe '#current_team_label' do
      it 'returns nil if the resource has no team label' do
        resource = resource_klass.new([])

        expect(resource.current_team_label).to be_nil
      end

      it 'returns the team name if the resource has a team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.team_labels.first)])

        expect(resource.current_team_label).to eq(DevopsLabels.team_labels.first)
      end
    end

    describe '#current_stage_name' do
      it 'returns nil if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource.current_stage_name).to be_nil
      end

      it 'returns the stage name if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.current_stage_name).to eq(DevopsLabels.stages.first)
      end
    end

    describe '#current_group_name' do
      it 'returns nil if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource.current_group_name).to be_nil
      end

      it 'returns the group name if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.current_group_name).to eq(DevopsLabels.groups.first)
      end
    end

    describe '#all_category_labels_for_stage' do
      it 'returns [] if the stage does not exists' do
        expect(resource.all_category_labels_for_stage(nil)).to eq([])
        expect(resource.all_category_labels_for_stage('')).to eq([])
        expect(resource.all_category_labels_for_stage('foo')).to eq([])
      end

      it 'returns the stage category labels when given a stage name' do
        expect(resource.all_category_labels_for_stage('release')).to include('Category:Continuous Delivery', 'Category:Review apps')
        expect(resource.all_category_labels_for_stage('release')).not_to include('logging')
      end
    end

    describe '#all_category_labels_for_group' do
      it 'returns [] if the group does not exists' do
        expect(resource.all_category_labels_for_stage(nil)).to eq([])
        expect(resource.all_category_labels_for_stage('')).to eq([])
        expect(resource.all_category_labels_for_stage('foo')).to eq([])
      end

      it 'returns the stage category labels when given a group name' do
        resource = resource_klass.new([])

        expect(resource.all_category_labels_for_group('progressive delivery')).to include('Category:Continuous Delivery')
        expect(resource.all_category_labels_for_group('progressive delivery')).not_to include('release governance')
      end
    end

    describe '#has_stage_label?' do
      it 'returns false if the resource has no stage label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_stage_label
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource).to be_has_stage_label
      end
    end

    describe '#has_group_label?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_group_label
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource).to be_has_group_label
      end
    end

    describe '#has_category_label_for_current_stage?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('Category:Continuous Delivery')])

        expect(resource).to be_has_category_label_for_current_stage
      end

      it 'returns true if the resource has a stage label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('devops::release'), label_klass.new('logging')])

        expect(resource).not_to be_has_category_label_for_current_stage
      end
    end

    describe '#has_category_label_for_current_group?' do
      it 'returns false if the resource has no group label' do
        resource = resource_klass.new([])

        expect(resource).not_to be_has_category_label_for_current_group
      end

      it 'returns true if the resource has a group label and a corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery'), label_klass.new('Category:Continuous Delivery')])

        expect(resource).to be_has_category_label_for_current_group
      end

      it 'returns false if the resource has a group label but no corresponding category label' do
        resource = resource_klass.new([label_klass.new('group::progressive delivery'), label_klass.new('release governance')])

        expect(resource).not_to be_has_category_label_for_current_group
      end
    end

    describe '#can_infer_labels?' do
      it 'returns false if the resource has no stage, group, category, or team label' do
        resource = resource_klass.new([])

        expect(resource.can_infer_labels?).to eq(false)
      end

      it 'returns true if the resource has a stage label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.stage_and_departments_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end

      it 'returns true if the resource has a group label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.group_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end

      it 'returns true if the resource has a category label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.category_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end

      it 'returns true if the resource has a team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.team_labels.first)])

        expect(resource.can_infer_labels?).to eq(true)
      end
    end

    describe '#single_team_label? & #can_infer_from_team_label?' do
      it 'returns false if the resource has no labels' do
        resource = resource_klass.new([])

        expect(resource).not_to be_single_team_label
        expect(resource).not_to be_can_infer_from_team_label
      end

      it 'returns true if the resource has a single team label' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.team_labels.first)])

        expect(resource).to be_single_team_label
        expect(resource).to be_can_infer_from_team_label
      end

      it 'returns true if the resource has several team labels' do
        resource = resource_klass.new([label_klass.new(DevopsLabels.team_labels.first), label_klass.new(DevopsLabels.team_labels.last)])

        expect(resource).not_to be_single_team_label
        expect(resource).not_to be_can_infer_from_team_label
      end
    end

    describe '#stage_has_a_single_group?' do
      it 'returns false if the stage does not exists' do
        expect(resource.stage_has_a_single_group?(nil)).to be(false)
        expect(resource.stage_has_a_single_group?('')).to be(false)
        expect(resource.stage_has_a_single_group?('foo')).to be(false)
      end

      it 'returns true if the give stage has a single group' do
        expect(resource.stage_has_a_single_group?('package')).to be(true)
      end

      it 'returns false if the resource has several team labels' do
        expect(resource.stage_has_a_single_group?('create')).to be(false)
      end
    end

    describe '#first_group_for_stage' do
      it 'returns false if the stage does not exists' do
        expect(resource.first_group_for_stage(nil)).to be_nil
        expect(resource.first_group_for_stage('')).to be_nil
        expect(resource.first_group_for_stage('foo')).to be_nil
      end

      it 'returns the first group of the given stage' do
        expect(resource.first_group_for_stage('enablement')).to eq('distribution')
      end
    end

    describe '#stage_for_group' do
      it 'returns nil if the no stage corresponds to the given group' do
        expect(resource.stage_for_group(nil)).to be_nil
        expect(resource.stage_for_group('')).to be_nil
        expect(resource.stage_for_group('foo')).to be_nil
      end

      it 'returns the stage name of the given group' do
        expect(resource.stage_for_group('distribution')).to eq('enablement')
      end
    end

    describe '#new_stage_and_group_labels_from_intelligent_inference and #comment_for_intelligent_stage_and_group_labels_inference' do
      where(:case_name, :infer_from_category, :current_labels, :expected_new_stage_and_group_labels_from_intelligent_inference, :explanation) do
        [
          ["stage: yes, group: yes, category: yes, team: yes => No new labels.", true, ["devops::configure", "group::orchestration", "wiki", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: yes, category: yes, team: no => No new labels.", true, ["devops::configure", "group::orchestration", "wiki"], [], ''],
          ["stage: yes, group: yes, category: yes, team: yes, no category inference => No new labels.", false, ["devops::configure", "group::orchestration", "wiki", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: yes, category: yes, team: no, no category inference => No new labels.", false, ["devops::configure", "group::orchestration", "wiki"], [], ''],

          ["stage: yes, group: yes, category: no, team: yes => No new labels.", true, ["devops::configure", "group::orchestration", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: yes, category: no, team: no => No new labels.", true, ["devops::configure", "group::orchestration"], [], ''],
          ["stage: yes, group: yes, category: no, team: yes, no category inference => No new labels.", false, ["devops::configure", "group::orchestration", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: yes, category: no, team: no, no category inference => No new labels.", false, ["devops::configure", "group::orchestration"], [], ''],

          ["stage: yes, group: no, category: yes, team: yes (100% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "Verify [DEPRECATED]"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: yes (100% matching stage, 2 different groups) => Manual triage required", true, ["devops::create", "wiki", "merge requests", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups) => Manual triage required", true, ["devops::create", "wiki", "merge requests"], [], ''],
          ["stage: yes, group: no, category: yes, team: yes (66% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "design management", "analytics", "Verify [DEPRECATED]"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"design management" ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "design management", "analytics"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"design management" ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: yes (50% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "analytics", "Verify [DEPRECATED]"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "analytics"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage) => Group based on stage since feature does not match stage and stage has only one group", true, ["devops::package", "design management", "analytics"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: yes (33% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "analytics", "epics", "Verify [DEPRECATED]"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on feature since feature matches stage", true, ["devops::create", "wiki", "analytics", "epics"], ["group::knowledge"], %(Setting ~"group::knowledge" based on ~"wiki".)],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Manual triage required since feature is less than 50%", true, ["devops::create", "search", "analytics", "epics"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage) => Group based on stage since feature does not match stage and stage has only one group", true, ["devops::package", "design management", "analytics", "epics"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: yes (none matching stage) => Group based on stage since feature does not match stage", true, ["devops::package", "design management", "Verify [DEPRECATED]"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Manual triage required since feature does not match stage but stage has several groups", true, ["devops::secure", "design management"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on stage since feature does not match stage and stage has only one group", true, ["devops::package", "design management"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage) => Group based on category since group has only one category", true, ["devops::create", "Category:Gitaly"], ["group::gitaly"], %(Setting ~"group::gitaly" based on ~"Category:Gitaly".)],
          ["stage: yes, group: no, category: yes, team: yes (100% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki"], [], ''],
          ["stage: yes, group: no, category: yes, team: yes (100% matching stage, 2 different groups), no category inference => Manual triage required", false, ["devops::create", "wiki", "merge requests", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (100% matching stage, 2 different groups), no category inference => Manual triage required", false, ["devops::create", "wiki", "merge requests"], [], ''],
          ["stage: yes, group: no, category: yes, team: yes (66% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "design management", "analytics", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (66% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "design management", "analytics"], [], ''],
          ["stage: yes, group: no, category: yes, team: yes (50% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "analytics", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "analytics"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (50% matching stage), no category inference => Group based on stage since feature does not match stage and stage has only one group", false, ["devops::package", "design management", "analytics"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: yes (33% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "analytics", "epics", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage), no category inference => Manual triage required", false, ["devops::create", "wiki", "analytics", "epics"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage), no category inference => Manual triage required since feature is less than 50%", false, ["devops::create", "search", "analytics", "epics"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (33% matching stage), no category inference => Group based on stage since feature does not match stage and stage has only one group", false, ["devops::package", "design management", "analytics", "epics"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: yes (none matching stage), no category inference => Group based on stage since feature does not match stage", false, ["devops::package", "design management", "Verify [DEPRECATED]"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],
          ["stage: yes, group: no, category: yes, team: no (none matching stage), no category inference => Manual triage required since feature does not match stage but stage has several groups", false, ["devops::secure", "design management"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage), no category inference => Manual triage required since feature does not match stage but stage has several groups", false, ["devops::secure", "design management"], [], ''],
          ["stage: yes, group: no, category: yes, team: no (none matching stage), no category inference => Group based on stage since feature does not match stage and stage has only one group", false, ["devops::package", "design management"], ["group::package"], %(Setting ~"group::package" based on ~"devops::package".)],

          ["stage: yes, group: no, category: no, team: yes => Group based on team", true, ["Gitaly [DEPRECATED]", "devops::configure"], ["group::gitaly"], %(Setting ~"group::gitaly" based on ~"Gitaly [DEPRECATED]".)],
          ["stage: yes, group: no, category: no, team: no => Manual triage required", true, ["devops::verify"], [], ''],
          ["stage: yes, group: no, category: no, team: yes, no category inference => Group based on team", false, ["Gitaly [DEPRECATED]", "devops::configure"], ["group::gitaly"], %(Setting ~"group::gitaly" based on ~"Gitaly [DEPRECATED]".)],
          ["stage: yes, group: no, category: no, team: no, no category inference => Manual triage required", false, ["devops::verify"], [], ''],

          ["stage: no, group: yes, category: yes, team: yes => Stage based on team", true, ["design management", "group::source code", "markdown", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: yes, category: yes, team: no => Stage based on group", true, ["design management", "group::source code", "markdown"], ["devops::create"], %(Setting ~"devops::create" based on ~"group::source code".)],
          ["stage: no, group: yes, category: yes, team: yes, no category inference => Stage based on team", false, ["design management", "group::source code", "markdown", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: yes, category: yes, team: no, no category inference => Stage based on group", false, ["design management", "group::source code", "markdown"], ["devops::create"], %(Setting ~"devops::create" based on ~"group::source code".)],

          ["stage: no, group: yes, category: no, team: yes => Stage label based on group", true, ["Gitaly [DEPRECATED]", "group::memory"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::memory".)],
          ["stage: no, group: yes, category: no, team: no => Stage label based on group", true, ["group::memory"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::memory".)],
          ["stage: no, group: yes, category: no, team: yes, no category inference => Stage label based on group", false, ["Gitaly [DEPRECATED]", "group::memory"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::memory".)],
          ["stage: no, group: yes, category: no, team: no, no category inference => Stage label based on group", false, ["group::memory"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::memory".)],

          ["stage: no, group: no, category: no, team: yes => Stage based on team", true, ["Plan [DEPRECATED]", "backend", "bug"], ["devops::plan",], %(Setting ~"devops::plan" based on ~"Plan [DEPRECATED]".)],
          ["stage: no, group: no, category: no, team: no => Manual triage required", true, ["bug", "rake tasks"], [], ''],
          ["stage: no, group: no, category: no, team: yes, no category inference => Stage based on team", false, ["Plan [DEPRECATED]", "backend", "bug"], ["devops::plan",], %(Setting ~"devops::plan" based on ~"Plan [DEPRECATED]".)],
          ["stage: no, group: no, category: no, team: no, no category inference => Manual triage required", false, ["bug", "rake tasks"], [], ''],

          ["stage: no, group: no, category: yes, team: yes (best match: 100%) => Stage based on team", true, ["internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on feature", true, ["internationalization"], ["devops::manage", "group::import"], %(Setting ~"devops::manage" ~"group::import" based on ~"internationalization".)],
          ["stage: no, group: no, category: yes, team: no (best match: 100%) => Stage and group based on category", true, ["Category:Source Code Management"], ["devops::create", "group::source code"], %(Setting ~"devops::create" ~"group::source code" based on ~"Category:Source Code Management".)],
          ["stage: no, group: no, category: yes, team: yes (best match: 66%) => Stage based on team label", true, ["snippets", "elasticsearch", "internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 66%) => Stage and group based on feature", true, ["snippets", "web ide", "internationalization"], ["devops::create", "group::editor"], %(Setting ~"devops::create" ~"group::editor" based on ~"web ide" ~"snippets".)],
          ["stage: no, group: no, category: yes, team: yes (best match: 50%) => Stage based on team", true, ["elasticsearch", "internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 50%) => Manual triage required", true, ["elasticsearch", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (best match: 33%) => Stage based on team", true, ["pipeline", "elasticsearch", "internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 33%) => Manual triage required", true, ["pipeline", "elasticsearch", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (no match) => Stage based on team", true, ["backstage", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (no match) => Manual triage required", true, ["backstage"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (best match: 100%), no category inference => Stage based on team", false, ["internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 100%), no category inference => Manual triage required", false, ["internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: no (best match: 100%), no category inference => Manual triage required", false, ["Category:Source Code Management"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (best match: 66%), no category inference => Stage based on team label", false, ["snippets", "elasticsearch", "internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 66%), no category inference => Manual triage required", false, ["snippets", "web ide", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (best match: 50%), no category inference => Stage based on team", false, ["elasticsearch", "internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 50%), no category inference => Manual triage required", false, ["elasticsearch", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (best match: 33%), no category inference => Stage based on team", false, ["pipeline", "elasticsearch", "internationalization", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (best match: 33%), no category inference => Manual triage required", false, ["pipeline", "elasticsearch", "internationalization"], [], ''],
          ["stage: no, group: no, category: yes, team: yes (no match), no category inference => Stage based on team", false, ["backstage", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, team: no (no match), no category inference => Manual triage required", false, ["backstage"], [], ''],

          ["~insights => ~Quality", true, ["insights"], ["Quality"], %(Setting ~"Quality" based on ~"insights".)],
          ["~Quality ~insights => No new labels", true, ["Quality", "insights"], [], ''],
          ["~insights => ~Quality, no category inference", false, ["insights"], [], ''],
          ["~Quality ~insights, no category inference => No new labels", false, ["Quality", "insights"], [], ''],

          ["stage: yes, group: yes, category: yes, single_team: yes => Different stages to legacy label. Do nothing", true, ["devops::configure", "group::orchestration", "wiki", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: yes, category: yes, single_team: yes => Stage and group already applied. Do nothing", true, ["group::geo", "devops::enablement"], [], ''],
          ["stage: yes, group: no, category: yes, single_team: yes => Group label based on team", true, ["Gitaly [DEPRECATED]", "devops::create"], ["group::gitaly"], %(Setting ~"group::gitaly" based on ~"Gitaly [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, single_team: yes => Stage label based on team", true, ["wiki", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: yes, category: yes, single_team: yes => Do not override existing group", true, ["group::orchestration", "wiki", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: yes, group: no, category: no, single_team: yes => Manual triage required", true, ["devops::configure", "Verify [DEPRECATED]"], [], ''],
          ["stage: no, group: no, category: no, single_team: yes => Apply stage and group label from legacy team label", true, ["Distribution [DEPRECATED]"], ["devops::enablement", "group::distribution",], %(Setting ~"devops::enablement" ~"group::distribution" based on ~"Distribution [DEPRECATED]".)],
          ["stage: no, group: no, category: no, single_team: yes => Apply stage and group label from legacy team label 2", true, ["Gitaly [DEPRECATED]"], ["devops::create", "group::gitaly"], %(Setting ~"devops::create" ~"group::gitaly" based on ~"Gitaly [DEPRECATED]".)],
          ["stage: no, group: yes, category: yes, single_team: no => Do nothing if a stage label is missing and multiple legacy labels present", true, ["group::orchestration", "wiki", "Verify [DEPRECATED]", "Configure [DEPRECATED]"], ["devops::configure"], %(Setting ~"devops::configure" based on ~"group::orchestration".)],
          ["stage: yes, group: yes, category: yes, single_team: yes, no category inference => Different stages to legacy label. Do nothing", false, ["devops::configure", "group::orchestration", "wiki", "Verify [DEPRECATED]"], [], ''],
          ["stage: yes, group: yes, category: yes, single_team: yes, no category inference => Stage and group already applied. Do nothing", false, ["group::geo", "devops::enablement"], [], ''],
          ["stage: yes, group: no, category: yes, single_team: yes, no category inference => Group label based on team", false, ["Gitaly [DEPRECATED]", "devops::create"], ["group::gitaly"], %(Setting ~"group::gitaly" based on ~"Gitaly [DEPRECATED]".)],
          ["stage: no, group: no, category: yes, single_team: yes, no category inference => Stage label based on team", false, ["wiki", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: no, group: yes, category: yes, single_team: yes, no category inference => Do not override existing group", false, ["group::orchestration", "wiki", "Verify [DEPRECATED]"], ["devops::verify"], %(Setting ~"devops::verify" based on ~"Verify [DEPRECATED]".)],
          ["stage: yes, group: no, category: no, single_team: yes, no category inference => Manual triage required", false, ["devops::configure", "Verify [DEPRECATED]"], [], ''],
          ["stage: no, group: no, category: no, single_team: yes, no category inference => Apply stage and group label from legacy team label", false, ["Distribution [DEPRECATED]"], ["devops::enablement", "group::distribution",], %(Setting ~"devops::enablement" ~"group::distribution" based on ~"Distribution [DEPRECATED]".)],
          ["stage: no, group: no, category: no, single_team: yes, no category inference => Apply stage and group label from legacy team label 2", false, ["Gitaly [DEPRECATED]"], ["devops::create", "group::gitaly"], %(Setting ~"devops::create" ~"group::gitaly" based on ~"Gitaly [DEPRECATED]".)],
          ["stage: no, group: yes, category: yes, single_team: no, no category inference => Do nothing if a stage label is missing and multiple legacy labels present", false, ["group::orchestration", "wiki", "Verify [DEPRECATED]", "Configure [DEPRECATED]"], ["devops::configure"], %(Setting ~"devops::configure" based on ~"group::orchestration".)],

          ["stage: yes, group: yes, category: no, single_category: yes => Apply Category label based on group", true, ["group::gitaly", "devops::create"], ["Category:Gitaly"], %(Setting ~"Category:Gitaly" based on ~"group::gitaly".)],
          ["stage: no, group: yes, category: no, single_category: yes => Apply Stage and Category label based on group", true, ["group::gitaly"], ["devops::create", "Category:Gitaly"], %(Setting ~"devops::create" ~"Category:Gitaly" based on ~"group::gitaly".)],
          ["stage: no, group: yes, category: no, single_category: no => Apply Stage based on group, cannot infer Category", true, ["group::geo"], ["devops::enablement"], %(Setting ~"devops::enablement" based on ~"group::geo".)],
          ["stage: yes, group: yes, category: no, single_category: no => Stage and Group already applied, cannot infer Category based on group. Do nothing", true, ["group::geo", "devops::enablement"], [], ''],
        ]
      end

      with_them do
        it "returns an array of relevant labels" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })

          expect(resource.new_stage_and_group_labels_from_intelligent_inference(infer_from_category: infer_from_category).new_labels).to eq expected_new_stage_and_group_labels_from_intelligent_inference
        end
      end

      it "returns nil if the resource does not warrant any new labels" do
        resource = resource_klass.new([])

        expect(resource.comment_for_intelligent_stage_and_group_labels_inference).to be_nil
      end

      with_them do
        it "returns a comment with a /label quick action" do
          resource = resource_klass.new(current_labels.map { |l| label_klass.new(l) })

          if expected_new_stage_and_group_labels_from_intelligent_inference.empty?
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: infer_from_category)).to be_nil
          else
            expect(resource.comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: infer_from_category)).to eq %(#{explanation}\n/label #{expected_new_stage_and_group_labels_from_intelligent_inference.map { |l| %(~"#{l}") }.join(' ')})
          end
        end
      end
    end
  end
end
