require 'spec_helper'
require 'gitlab/triage/engine'
require 'versioned_milestone'

RSpec.describe VersionedMilestone do
  subject { described_class.new(context) }

  let(:context) do
    Gitlab::Triage::Resource::Context.build(resource, redact_confidentials: false)
  end

  let(:resource) { { type: 'issue', milestone: milestone_resource } }
  let(:milestone_resource) {}

  let(:all_active) do
    [
      { title: '12.0' },
      { title: '12.1' },
      { title: '12.2' },
      { title: '12.3' },
      { title: 'Backlog' },
      { title: 'FY21' }
    ].map(&Gitlab::Triage::Resource::Milestone.method(:new))
  end

  before do
    allow(subject).to receive(:all_active).and_return(all_active)
  end

  describe '#beyond_n_plus_1_milestone?' do
    context 'when milestone set to the current active' do
      let(:milestone_resource) { all_active[0].resource }

      it 'returns false' do
        expect(subject.beyond_n_plus_1_milestone?).to eq(false)
      end
    end

    context 'when milestone set to the next active' do
      let(:milestone_resource) { all_active[1].resource }

      it 'returns false' do
        expect(subject.beyond_n_plus_1_milestone?).to eq(false)
      end
    end

    context 'when milestone set to plus two' do
      let(:milestone_resource) { all_active[2].resource }

      it 'returns true' do
        expect(subject.beyond_n_plus_1_milestone?).to eq(true)
      end
    end

    context 'when the resource is confidential with next active milestone' do
      let(:resource) do
        { type: 'issue', milestone: milestone_resource, confidential: true }
      end
      let(:milestone_resource) { all_active[1].resource }

      it 'returns false' do
        expect(subject.beyond_n_plus_1_milestone?).to eq(false)
      end
    end
  end

  describe '#all' do
    it 'does not return Backlog nor FY21' do
      expect(subject.all).not_to include('Backlog')
      expect(subject.all).not_to include('FY21')
    end
  end
end
