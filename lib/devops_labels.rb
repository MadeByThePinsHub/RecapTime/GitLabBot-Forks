# frozen_string_literal: true

require_relative 'www_gitlab_com'

class DevopsLabels
  CATEGORIES_PER_DEPARTMENT = {
    'Quality' => [
      'ci-build',
      'insights',
      'master:broken',
      'master:flaky',
      'master:needs-investigation',
      'QA',
      'static analysis'
    ]
  }
  TEAM_TO_STAGE = {
    'Manage [DEPRECATED]' => 'manage',
    'Plan [DEPRECATED]' => 'plan',
    'Create [DEPRECATED]' => 'create',
    'Verify [DEPRECATED]' => 'verify',
    'Package [DEPRECATED]' => 'package',
    'Release [DEPRECATED]' => 'release',
    'Configure [DEPRECATED]' => 'configure',
    'Serverless [DEPRECATED]' => 'configure',
    'Monitor [DEPRECATED]' => 'monitor',
    'Secure [DEPRECATED]' => 'secure',
    'Defend [DEPRECATED]' => 'defend',
    'Growth [DEPRECATED]' => 'growth'
  }
  TEAM_TO_GROUP = {
    'Gitaly [DEPRECATED]' => 'gitaly',
    'Gitter [DEPRECATED]' => 'gitter',
    'Distribution [DEPRECATED]' => 'distribution',
    'Geo [DEPRECATED]' => 'geo',
    'Memory [DEPRECATED]' => 'memory',
    'Ecosystem [DEPRECATED]' => 'ecosystem'
  }
  STAGE_LABEL_PREFIX = 'devops::'
  GROUP_LABEL_PREFIX = 'group::'
  CATEGORY_LABEL_PREFIX = 'Category:'

  def self.stages
    WwwGitLabCom.stages_from_www.keys
  end

  def self.groups
    groups_per_stage.values.flatten
  end

  def self.category_labels_for_www_categories(categories)
    www_category_keys = (categories || []).select do |category_key|
      WwwGitLabCom.categories_from_www.key?(category_key)
    end

    www_category_keys.each_with_object([]) do |category_key, memo|
      category = WwwGitLabCom.categories_from_www[category_key]
      memo <<
        if category.key?('label')
          category['label']
        else
          "#{CATEGORY_LABEL_PREFIX}#{category['name']}"
        end
      memo << category['feature_labels'] if category.key?('feature_labels')
    end.flatten
  end

  private_class_method :category_labels_for_www_categories

  def self.groups_per_stage
    @groups_per_stage ||=
      WwwGitLabCom.stages_from_www.each_with_object({}) do |(stage, attrs), memo|
        memo[stage] = attrs['groups'].each_key.map { |group| group.tr('_', ' ') }
      end
  end

  def self.categories_per_group
    @categories_per_group ||=
      WwwGitLabCom.groups_from_www.each_with_object({}) do |(group, attrs), memo|
        group_key = group.tr('_', ' ')
        memo[group_key] = attrs.fetch('feature_labels', [])
        memo[group_key].concat(category_labels_for_www_categories(attrs['categories']))
      end
  end

  def self.categories_per_group_or_department
    categories_per_group.merge(CATEGORIES_PER_DEPARTMENT)
  end

  def self.stage_and_departments_labels
    stages.map do |stage|
      "#{STAGE_LABEL_PREFIX}#{stage}"
    end + departments
  end

  def self.group_labels
    groups.map do |group|
      "#{GROUP_LABEL_PREFIX}#{group}"
    end
  end

  def self.category_labels
    categories_per_group.values.flatten +
      CATEGORIES_PER_DEPARTMENT.values.flatten
  end

  def self.team_labels
    TEAM_TO_STAGE.keys + TEAM_TO_GROUP.keys
  end

  def self.departments
    CATEGORIES_PER_DEPARTMENT.keys
  end

  def self.department?(item)
    departments.include?(item)
  end

  module Context
    extend self

    LabelsInferenceResult = Struct.new(:new_labels, :matching_labels) do
      def any?
        new_labels.any?
      end

      def new_labels
        self[:new_labels] ? Array[self[:new_labels]].flatten.compact : []
      end

      def matching_labels
        self[:matching_labels] ? Array[self[:matching_labels]].flatten : []
      end

      def comment
        "#{explanation}\n#{quick_actions}"
      end

      def quick_actions
        "/label #{new_labels_markdown}"
      end

      def self.merge(*label_inference_results)
        new_labels = label_inference_results.map(&:new_labels).uniq
        matching_labels = label_inference_results.map(&:matching_labels).uniq

        self.new(new_labels, matching_labels)
      end

      private

      def new_labels_markdown
        new_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def matching_labels_markdown
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end

      def explanation
        "Setting #{new_labels_markdown} based on #{matching_labels_markdown}." if any?
      end
    end

    MatchingGroup = Struct.new(:name, :matching_labels) do
      def in_stage?(stage)
        Context.stage_for_group(name) == stage
      end

      def markdown_labels
        matching_labels.map(&Context.method(:markdown_label)).join(' ')
      end
    end

    def label_names
      @label_names ||= labels.map(&:name)
    end

    def current_stage_label
      label_names.find { |label| label.start_with?(STAGE_LABEL_PREFIX) } ||
        (DevopsLabels.stage_and_departments_labels & label_names).first
    end

    def current_group_label
      label_names.find { |label| label.start_with?(GROUP_LABEL_PREFIX) }
    end

    def current_category_labels
      DevopsLabels.category_labels & label_names
    end

    def current_team_label
      (DevopsLabels.team_labels & label_names).first
    end

    def current_stage_name
      return unless current_stage_label

      current_stage_label.delete_prefix(DevopsLabels::STAGE_LABEL_PREFIX)
    end

    def current_group_name
      return unless current_group_label

      current_group_label.delete_prefix(DevopsLabels::GROUP_LABEL_PREFIX)
    end

    def all_category_labels_for_stage(stage)
      return [] unless DevopsLabels.stages.include?(stage)

      DevopsLabels.groups_per_stage[stage].flat_map do |group|
        all_category_labels_for_group(group)
      end
    end

    def all_category_labels_for_group(group)
      DevopsLabels.categories_per_group.fetch(group, [])
    end

    def has_stage_label?
      !current_stage_label.nil?
    end

    def has_group_label?
      !current_group_label.nil?
    end

    def has_category_label_for_current_stage?
      return false unless has_stage_label?

      !(all_category_labels_for_stage(current_stage_name) & label_names).empty?
    end

    def has_category_label_for_current_group?
      return false unless has_group_label?

      !(all_category_labels_for_group(current_group_name) & label_names).empty?
    end

    def can_infer_labels?
      !!(current_stage_label || current_group_label || !current_category_labels.empty? || current_team_label)
    end

    def single_team_label?
      (DevopsLabels.team_labels & label_names).one?
    end

    alias_method :can_infer_from_team_label?, :single_team_label?

    def stage_has_a_single_group?(stage)
      return false unless DevopsLabels.groups_per_stage.key?(stage)

      DevopsLabels.groups_per_stage[stage].one?
    end

    def first_group_for_stage(stage)
      DevopsLabels.groups_per_stage[stage]&.first
    end

    def stage_for_group(group)
      detected_stage = DevopsLabels.groups_per_stage.detect do |stage, groups|
        groups.include?(group)
      end

      detected_stage&.first
    end

    def new_stage_and_group_labels_from_intelligent_inference(infer_from_category: true)
      new_labels_from_single_team_label = infer_stage_and_group_label_from_team_label

      return new_labels_from_single_team_label if new_labels_from_single_team_label.any?

      new_labels =
        if has_stage_label?
          if has_group_label?
            infer_category_from_group
          else
            if infer_from_category
              infer_group_from_category
            else
              infer_group_from_stage
            end
          end
        else
          if has_group_label?
            infer_stage_and_category_from_group
          else
            infer_stage_and_group_from_category if infer_from_category
          end
        end

      new_labels || LabelsInferenceResult.new
    end

    def comment_for_intelligent_stage_and_group_labels_inference(infer_from_category: true)
      new_stage_and_group_labels = new_stage_and_group_labels_from_intelligent_inference(infer_from_category: infer_from_category)

      new_stage_and_group_labels.comment if new_stage_and_group_labels.any?
    end

    private

    def infer_group_from_stage
      return unless stage_has_a_single_group?(current_stage_name)

      LabelsInferenceResult.new(label_for(group: first_group_for_stage(current_stage_name)), current_stage_label)
    end

    def infer_group_from_category
      matching_group = best_matching_group

      if matching_group && matching_group.in_stage?(current_stage_name)
        LabelsInferenceResult.new(label_for(group: matching_group.name), matching_group.matching_labels)
      elsif stage_matching_groups.none?
        infer_group_from_stage
      end
    end

    def infer_stage_and_group_from_category
      matching_group = best_matching_group
      return unless matching_group

      stage_label = label_for(stage: stage_for_group(matching_group.name))
      group_label = label_for(group: matching_group.name)

      LabelsInferenceResult.new([stage_label, group_label], matching_group.matching_labels)
    end

    def infer_stage_and_group_label_from_team_label
      return LabelsInferenceResult.new unless single_team_label?

      group = group_from_team_label unless has_group_label?
      stage = group ? stage_for_group(group) : stage_from_team_label unless has_stage_label?

      if stage || group
        stage_label = label_for(stage: stage)
        group_label = label_for(group: group) unless has_group_label?

        LabelsInferenceResult.new([stage_label, group_label], current_team_label)
      else
        LabelsInferenceResult.new
      end
    end

    def infer_category_from_group
      category_labels = all_category_labels_for_group(current_group_name).filter do |category|
        category.start_with?(DevopsLabels::CATEGORY_LABEL_PREFIX)
      end

      if category_labels.one?
        LabelsInferenceResult.new(category_labels.first, current_group_label)
      else
        LabelsInferenceResult.new
      end
    end

    def infer_stage_from_group
      stage_label = label_for(stage: stage_for_group(current_group_name))

      LabelsInferenceResult.new(stage_label, current_group_label)
    end

    def infer_stage_and_category_from_group
      inferred_stage = infer_stage_from_group
      inferred_category = infer_category_from_group

      LabelsInferenceResult.merge(inferred_stage, inferred_category)
    end

    def best_matching_group
      matching_groups = stage_matching_groups.any? ? stage_matching_groups : groups_from_category
      matching_labels_count = matching_groups.map(&:matching_labels).size.to_f

      matching_groups.find do |matching_group|
        (matching_group.matching_labels.size / matching_labels_count) > 0.5
      end
    end

    def groups_from_category
      @groups_from_category ||=
        DevopsLabels.categories_per_group_or_department.each_with_object([]) do |(group, category_labels), matches|
          matching_category_labels = category_labels & label_names
          matches << MatchingGroup.new(group, matching_category_labels) if matching_category_labels.any?
        end
    end

    def stage_matching_groups
      @stage_matching_groups ||=
        groups_from_category.select do |matching_group|
          DevopsLabels.department?(matching_group.name) ||
            stage_for_group(matching_group.name) == current_stage_name
        end
    end

    def stage_from_team_label
      TEAM_TO_STAGE.find do |(team, _), matches|
        label_names.include?(team)
      end&.last
    end

    def group_from_team_label
      TEAM_TO_GROUP.find do |(team, _), matches|
        label_names.include?(team)
      end&.last
    end

    def label_for(stage: nil, group: nil)
      stage_or_group = stage || group
      return stage_or_group if DevopsLabels.department?(stage_or_group)

      if stage
        "#{DevopsLabels::STAGE_LABEL_PREFIX}#{stage}"
      elsif group
        "#{DevopsLabels::GROUP_LABEL_PREFIX}#{group}"
      end
    end

    def markdown_label(label)
      %(~"#{label}")
    end
  end
end
